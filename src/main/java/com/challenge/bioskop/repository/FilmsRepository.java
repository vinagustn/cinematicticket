package com.challenge.bioskop.repository;

import com.challenge.bioskop.model.Films;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface FilmsRepository extends JpaRepository<Films, Integer> {

    public Films findByFilmCode(String filmCode);

    Films findFilmsByFilmName(String filmName);

    public Films findByFilmId(Integer filmId);

    @Query(value = "select * from films f where f.is_show = true", nativeQuery = true)
    List<Films> findAllByIsShowTrue();
}
