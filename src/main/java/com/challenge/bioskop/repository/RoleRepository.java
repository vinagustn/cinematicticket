package com.challenge.bioskop.repository;

import com.challenge.bioskop.enumeration.ERole;
import com.challenge.bioskop.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Integer> {
    Optional<Roles> findByName(ERole name);
}
