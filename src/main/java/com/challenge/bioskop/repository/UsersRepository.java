package com.challenge.bioskop.repository;

import com.challenge.bioskop.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface UsersRepository extends JpaRepository<Users, Integer> {
    Boolean existsByUserName(String username);
    Boolean existsByEmail(String email);

    public Users findByUserName(String userName);

}

