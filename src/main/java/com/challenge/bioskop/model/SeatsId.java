package com.challenge.bioskop.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "seat_code"))
public class SeatsId implements Serializable {
    @Column(name = "studio_name")
    private Character studioName;

    @Column(name = "seat_code")
    private String seatCode;

    public SeatsId(Character studioName, String seatCode){
        this.studioName = studioName;
        this.seatCode = seatCode;
    }
}
