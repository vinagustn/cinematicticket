package com.challenge.bioskop.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity(name = "schedules")
public class Schedules {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedules_id")
    private Integer schedulesId;

    @ManyToOne(targetEntity = Films.class)
    @JoinColumn(name = "film_id", nullable = false)
    private Films filmId;

    @Column(name = "date_show")
    private String dateShow;

    @Column(name = "start_time")
    private String startTime;

    @Column(name = "end_time")
    private String endTime;

    @Column(name = "price")
    private Integer price;

}
