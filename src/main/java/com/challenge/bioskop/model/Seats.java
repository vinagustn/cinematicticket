package com.challenge.bioskop.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity(name = "seats")
@IdClass(SeatsId.class)
public class Seats implements Serializable {
    @Id
    private Character studioName;

    @Id
    private String seatCode;
}