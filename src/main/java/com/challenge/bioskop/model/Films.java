package com.challenge.bioskop.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity(name = "films")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "film_name"))
public class Films {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "film_id")
    private Integer filmId;

    @Column(name = "film_code")
    private String filmCode;
    
    @Column(name = "film_name")
    private String filmName;
    
    @Column(name = "is_show")
    private Boolean isShow;
    
}
