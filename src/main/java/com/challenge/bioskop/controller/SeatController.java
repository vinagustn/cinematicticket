package com.challenge.bioskop.controller;

import com.challenge.bioskop.model.Seats;
import com.challenge.bioskop.service.SeatService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Seats", description = "API to create "+
        "seats data from Seats Schema in Challenge Database")
@Controller
@RestController
@RequestMapping(value = "/seats")
public class SeatController {

    @Autowired
    private SeatService seatService;

    //add seats
    @ApiResponses(value = {
            @ApiResponse(responseCode = "226", description = "Seats added successfully!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{"+
                                    "\"studioName\":\"A\"," +
                                    "\"seatCode\":\"3H\"" +
                                    "}"))
                    }),
            @ApiResponse(responseCode = "444", description = "Add seats failed!",
                    content = { @Content})})
    @Operation(summary = "add seats code and studio name to Seats Entity")
    @PostMapping(value = "/customer/add-seat")
    public ResponseEntity<String>  addSeats(@Schema(example = "{"+
            "\"studioName\":\"A\"," +
            "\"seatCode\":\"3H\"" +
            "}")
                                  @RequestBody Seats seats){
        seatService.addSeat(seats);
        return new ResponseEntity<>(seats.toString(), HttpStatus.CREATED);
    }
}
