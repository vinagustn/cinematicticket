package com.challenge.bioskop.controller;

import com.challenge.bioskop.service.InvoiceService;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Tag(name = "Invoice", description = "API to generate ticket into pdf file")
@RestController
@RequestMapping("/invoice")
@Controller
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    //generating cinema ticket into pdf file
    @GetMapping("/customer/get-ticket")
    public void getCinemaTicket(HttpServletResponse response) throws IOException, JRException{
        JasperReport sourceFileName = JasperCompileManager
                .compileReport(ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX
                                + "ticket.jrxml").getAbsolutePath());

        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
                invoiceService.generateInvoice("vinagustn", "Falling Into Your Smile", 1, "3H"));
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Author", "Vina Agustina");
        JasperPrint jasperPrint = JasperFillManager.fillReport(sourceFileName, parameters, beanCollectionDataSource);

        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "inline; filename=ticket.pdf");

        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }
}
