package com.challenge.bioskop.controller;

import com.challenge.bioskop.service.ScheduleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Tag(name = "Schedules", description = "API to create schedule data from Schedules Schema in Challenge Database")
@Controller
@RestController
@RequestMapping(value = "/schedules")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    //add schedules
    @ApiResponses(value = {
            @ApiResponse(responseCode = "226", description = "New schedule added successfully!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" + "\"filmId\":\"1\","+
                                    "\"dateShow\":\"2022-08-25\","+
                                    "\"startTime\":\"19:00\"," +
                                    "\"endTime\":\"20:30\"," +
                                    "\"price\":\"35000\"}"))
                    }),
            @ApiResponse(responseCode = "444", description = "Add new schedule failed!",
                    content = { @Content})})
    @PostMapping(value = "/admin/add-schedule")
    public ResponseEntity<String>  addSchedules(@Schema(example = "{" + "\"filmId\":\"1\","+
            "\"dateShow\":\"2022-08-25\","+
            "\"startTime\":\"19:00\"," +
            "\"endTime\":\"20:30\"," +
            "\"price\":\"35000\"}")
            @RequestBody Map<String, Object> schedules){
        scheduleService.addSchedule(
                schedules.get("filmId").toString(),
                schedules.get("dateShow").toString(),
                schedules.get("startTime").toString(),
                schedules.get("endTime").toString(),
                schedules.get("price").toString()
        );
        return new ResponseEntity<>(schedules.toString(), HttpStatus.CREATED);
    }

    //get Schedule
    @Operation(summary = "get a schedule by ScheduleId")
    @GetMapping(value = "/public/find-schedule-byId/{id}")
    public void findSchedulesByScheduleId(@PathVariable("id") Integer scheduleId){
        scheduleService.getSchedulesByScheduleId(scheduleId);
    }

}
