package com.challenge.bioskop.controller;

import com.challenge.bioskop.model.Users;
import com.challenge.bioskop.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/* HTTPS STATUS :
 * POST : CREATED
 * PUT & DELETE : ACCEPTED
 * GET : FOUND */

@Tag(name = "Users", description = "API to CRUD user data from Users Schema in Challenge Database")
@Controller
@RestController
@RequestMapping(value = "/users")
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    //add users
    @ApiResponses(value = {
            @ApiResponse(responseCode = "226", description = "New users added successfully!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{"+ "\"userId\":\"userId\"," +
                                    "\"userName\":\"vinagustn_\"," +
                                    "\"email\":\"vinagtn25@gmail.com\"," +
                                    "\"password\":\"pinakio123\"" +
                                    "}"))
                    }),
            @ApiResponse(responseCode = "444", description = "Add new users failed!",
                    content = { @Content})})
    @Operation(summary = "add user to Users Entity")
    @PostMapping(value = "/public/add-user")
    public ResponseEntity<String>  addUser(@Schema(example = "{"+ "\"userId\":\"userId\"," +
            "\"userName\":\"vinagustn_\"," +
            "\"email\":\"vinagtn25@gmail.com\"," +
            "\"password\":\"pinakio123\"" +
            "}")
            @RequestBody Users user){
        userService.saveUser(user);
        return new ResponseEntity<>(user.toString(), HttpStatus.CREATED);
    }

    //update users
    @ApiResponses(value = {
            @ApiResponse(responseCode = "226", description = "New users added successfully!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{"+
                                    "\"userId\":\"1\"," +
                                    "\"userName\":\"pinaaa\"," +
                                    "\"email\":\"vinagstn@gmail.com\"," +
                                    "\"password\":\"pina123\"" +
                                    "}"))
                    }),
            @ApiResponse(responseCode = "444", description = "Add new users failed!",
                    content = { @Content})})
    @Operation(summary = "update all columns of user by userId")
    @PutMapping(value = "/customer/update-user")
    public ResponseEntity<String>  update(@Schema(example = "{"+
            "\"userId\":\"1\"," +
            "\"userName\":\"pinaaa\"," +
            "\"email\":\"vinagstn@gmail.com\"," +
            "\"password\":\"pina123\"" +
            "}") @RequestBody Users user){
        userService.updateAll(user);
        return new ResponseEntity<>(user.toString(), HttpStatus.ACCEPTED);
    }

    //delete users
    @Operation(summary = "delete user by userId")
    @DeleteMapping(value = "/customer/delete-user/{userId}")
    public ResponseEntity<String>  delete(@Schema(example = "{ \"userId\":\"1\"}" ) @PathVariable("userId") Integer userId){
        return new ResponseEntity<>("User with id "+ userId + " success deleted", HttpStatus.ACCEPTED);
    }

    @GetMapping("/get-username/{username}")
    public ResponseEntity<String>  getUserByUserName(@Schema(example = "Fill in name") @PathVariable String username) {
        Users users = userService.getByUserName(username);

        LOG.info("Get username : {}", users.getUserName());

        Map<String, Object> respBody = new HashMap<>();
        respBody.put("ID User", users.getUserId());
        respBody.put("Nama Lengkap", users.getUserName());
        respBody.put("Email", users.getEmail());
        return new ResponseEntity<>(respBody.toString(), HttpStatus.FOUND);
    }


}
