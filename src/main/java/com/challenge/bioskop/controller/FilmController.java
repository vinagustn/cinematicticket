package com.challenge.bioskop.controller;

import com.challenge.bioskop.model.Films;
import com.challenge.bioskop.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* HTTPS STATUS :
* POST : CREATED
* PUT & DELETE : ACCEPTED
* GET : FOUND */

@Tag(name = "Films", description = "API to create, read, update, and delete "+
"film data from Films Schema in Challenge Database")
@Controller
@RestController
@RequestMapping(value = "/films")
public class FilmController {

    @Autowired
    private FilmService filmService;

    //add film
    @ApiResponses(value = {
            @ApiResponse(responseCode = "226", description = "New film added successfully!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{"+
                                    "\"filmId\":\"filmId\"," +
                                    "\"filmCode\":\"IMPFT001\"," +
                                    "\"filmName\":\"Imperfect\"," +
                                    "\"isShow\":\"true\"" +
                                    "}"))
                    }),
            @ApiResponse(responseCode = "444", description = "Add new film failed!",
                    content = { @Content})})
    @Operation(summary = "add film to Films Entity")
    @PostMapping(value = "/admin/add-film")
    public ResponseEntity<String>  addFilm(@Schema(example = "{"+
            "\"filmId\":\"filmId\"," +
            "\"filmCode\":\"IMPFT001\"," +
            "\"filmName\":\"Imperfect\"," +
            "\"isShow\":\"true\"" +
            "}")
            @RequestBody Films film){
        filmService.addFilm(film);
        return new ResponseEntity<>(film.toString(), HttpStatus.CREATED);
    }

    //update film
    @ApiResponses(value = {
            @ApiResponse(responseCode = "226", description = "Film updated successfully!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{"+
                                    "\"filmId\":\"1\"," +
                                    "\"filmCode\":\"AFT001\"," +
                                    "\"filmName\":\"After\"," +
                                    "\"isShow\":\"false\"" +
                                    "}"))
                    }),
            @ApiResponse(responseCode = "444", description = "Update film failed!",
                    content = { @Content})})
    @Operation(summary = "API to update Film (filmCode, filmName, isShow) by filmId ")
    @PutMapping(value = "/admin/update-film")
    public ResponseEntity<String>  updateFilmName(@Schema(example = "{"+
            "\"filmId\":\"1\"," +
            "\"filmCode\":\"AFT001\"," +
            "\"filmName\":\"After\"," +
            "\"isShow\":\"false\"" +
            "}")
            @RequestBody Films film){
        filmService.updateFilmName(film);
        return new ResponseEntity<>(film.toString(), HttpStatus.ACCEPTED);
    }

    //delete film
    @Operation(summary = "delete film by filmId")
    @DeleteMapping(value = "/admin/delete-film/{id}")
    public ResponseEntity<String> deleteFilm(@PathVariable("id") Integer filmId){
        filmService.deleteFilmById(filmId);
        return new ResponseEntity<>("Film with id " + filmId + " success deleted.", HttpStatus.ACCEPTED);
    }

    //get film when film is on showing
    @Operation(summary = "find film where film is showing now")
    @GetMapping(value = "/public/find-film-isShow")
    public ResponseEntity<String>  findAllByIsShowTrue(){
        List<Films> filmsList = filmService.getFilmIsShowTrue();
        return new ResponseEntity<>(filmsList.toString(), HttpStatus.FOUND);
    }

    //get film when film is on showing
    @Operation(summary = "find film by filmName")
    @GetMapping(value = "/public/find-film/{filmName}")
    public ResponseEntity<String> findFilmByFilmName(@PathVariable String filmName){
        Films films = filmService.getFilmByFilmName(filmName);

        Map<String, Object> respBody = new HashMap<>();
        respBody.put("Film ID", films.getFilmCode());
        respBody.put("Nama Film", films.getFilmName());
        respBody.put("Show", films.getIsShow());
        return new ResponseEntity<>(respBody.toString(), HttpStatus.FOUND);
    }
}