package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Users;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    Users saveUser(Users user);

    Users updateAll(Users user);

    Users getByUserName(String userName);

    void deleteById(Integer userId);

    List<Users> getAllUsers();

}

