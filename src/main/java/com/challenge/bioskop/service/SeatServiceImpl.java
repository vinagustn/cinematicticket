package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Seats;
import com.challenge.bioskop.repository.SeatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeatServiceImpl implements SeatService{

    @Autowired
    private SeatsRepository seatsRepository;

    @Override
    public Seats addSeat(Seats seats) {
        seats.getSeatCode();
        seats.getStudioName();
        return seatsRepository.save(seats);
    }

    @Override
    public Seats getSeatsBySeatCode(String seatCode) {
        return seatsRepository.findSeatsBySeatCode(seatCode);
    }
}
