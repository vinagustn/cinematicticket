package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Films;
import com.challenge.bioskop.model.Schedules;
import com.challenge.bioskop.repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleServiceImpl implements ScheduleService{

    @Autowired
    private SchedulesRepository schedulesRepository;

    @Autowired
    private FilmService filmService;

    @Autowired
    public ScheduleServiceImpl(SchedulesRepository schedulesRepository){this.schedulesRepository = schedulesRepository;}

    @Override
    public void addSchedule(Object filmId, Object dateShow, Object startTime, Object endTime, Object price) {
        Schedules schedules = new Schedules();
        schedules.setDateShow(dateShow.toString());
        schedules.setStartTime(startTime.toString());
        schedules.setEndTime(endTime.toString());
        schedules.setPrice(Integer.parseInt(price.toString()));

        Films film = filmService.getFilmByFilmId(Integer.parseInt(filmId.toString()));
        if(film != null){
            schedules.setFilmId(film);
        }else{
            filmService.addFilm(film);
            schedules.setFilmId(filmService.getFilmByFilmId(Integer.parseInt(filmId.toString())));
        }
        schedulesRepository.save(schedules);
    }

    @Override
    public Schedules getSchedulesByScheduleId(Integer scheduleId) {
        return schedulesRepository.findSchedulesBySchedulesId(scheduleId);
    }
}
