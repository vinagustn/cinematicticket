package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Schedules;
import org.springframework.stereotype.Service;

@Service
public interface ScheduleService {
    void addSchedule(Object filmId, Object dateShow, Object startTime, Object endTime, Object price);

    Schedules getSchedulesByScheduleId(Integer scheduleId);
}
