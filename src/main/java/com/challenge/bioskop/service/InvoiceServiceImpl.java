package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Seats;
import com.challenge.bioskop.model.Films;
import com.challenge.bioskop.model.Schedules;
import com.challenge.bioskop.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InvoiceServiceImpl implements InvoiceService{
    @Autowired
    private UserService userService;

    @Autowired
    private FilmService filmService;

    @Autowired
    private SeatService seatService;

    @Autowired
    private ScheduleService scheduleService;

    @Override
    public List<Map<String, String>> generateInvoice(String userName, String filmName, Integer scheduleId, String seatCode) {
        List<Map<String, String>> mapList = new ArrayList<>();
        Map<String, String> data = new HashMap<>();

        Users users = userService.getByUserName(userName);
        data.put("userName", users.getUserName());

        Films films = filmService.getFilmByFilmName(filmName);
        data.put("filmName", films.getFilmName());

        Schedules schedules = scheduleService.getSchedulesByScheduleId(scheduleId);
        data.put("dateShow", schedules.getDateShow());
        data.put("startTime", schedules.getStartTime());
        data.put("price", schedules.getPrice().toString());

        Seats seats = seatService.getSeatsBySeatCode(seatCode);
        data.put("studioName", seats.getStudioName().toString());
        data.put("seatCode", seats.getSeatCode());

        mapList.add(data);
        return mapList;
    }
}
