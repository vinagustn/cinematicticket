package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Films;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {

    Films addFilm(Films film);

    Films updateFilmName(Films film);

    void deleteFilmById(Integer filmId);

    List<Films> getFilmIsShowTrue();

    Films getFilmByFilmName(String filmName);

    Films getFilmByFilmId(Integer filmId);

    List<Films> getAllFilms();

}
