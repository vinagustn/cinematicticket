package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Films;
import com.challenge.bioskop.repository.FilmsRepository;
import com.challenge.bioskop.repository.SchedulesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmServiceImpl implements FilmService{

    @Autowired
    private FilmsRepository filmsRepository;

    @Autowired
    private SchedulesRepository schedulesRepository;

    @Autowired
    public FilmServiceImpl(FilmsRepository filmsRepository){this.filmsRepository = filmsRepository;}

    @Override
    public Films addFilm(Films film) {
        film.getFilmCode();
        film.getFilmName();
        film.getIsShow();
        return filmsRepository.save(film);
    }

    private static final Logger LOG = LoggerFactory.getLogger(FilmServiceImpl.class);

    @Override
    public Films updateFilmName(Films film) {
        film.getFilmCode();
        Films update = filmsRepository.findByFilmId(film.getFilmId());
        update.setFilmCode(film.getFilmCode());
        update.setFilmName(film.getFilmName());
        update.setIsShow(film.getIsShow());
        return filmsRepository.save(film);
    }

    @Override
    public void deleteFilmById(Integer filmId) {
        filmsRepository.deleteById(filmId);
    }

    @Override
    public List<Films> getFilmIsShowTrue() {
        List<Films> filmsList = filmsRepository.findAllByIsShowTrue();
        filmsList.forEach(films -> LOG.info(filmsList.toString()));
        return filmsList;
    }

    @Override
    public Films getFilmByFilmName(String filmName) {
        return filmsRepository.findFilmsByFilmName(filmName);
    }

    @Override
    public Films getFilmByFilmId(Integer filmId) {
        return filmsRepository.findByFilmId(filmId);
    }

    @Override
    public List<Films> getAllFilms() {
        return (filmsRepository.findAll());
    }
}
