package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Users;
import com.challenge.bioskop.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UserService{

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository){
        this.usersRepository = usersRepository;
    }

    @Override
    public Users saveUser(Users user) {
        user.getUserName();
        user.getEmail();
        user.getPassword();
        return usersRepository.save(user);
    }

    @Override
    public Users updateAll(Users user) {
        user.getUserName();
        Users update = usersRepository.findById(user.getUserId()).get();
        update.setUserName(user.getUserName());
        update.setEmail(user.getEmail());
        update.setPassword(user.getPassword());
        return usersRepository.save(user);
    }

    @Override
    public Users getByUserName(String userName) {
        return usersRepository.findByUserName(userName);
    }

    @Override
    public void deleteById(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    }


}
