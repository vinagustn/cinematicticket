package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Seats;
import org.springframework.stereotype.Service;

@Service
public interface SeatService {
    Seats addSeat(Seats seats);

    Seats getSeatsBySeatCode(String seatCode);
}
