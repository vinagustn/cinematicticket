package com.challenge.bioskop.service;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface InvoiceService {
    List<Map<String, String>> generateInvoice(String userName, String filmName, Integer scheduleId, String seatCode);
}
