package com.challenge.bioskop.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SwaggerConfiguration {
    @Bean
    public OpenAPI customeSwaggerAPI(@Value("Swagger for CRUD cinema ticket (user, film, and schedule)") String appDescription,
                                     @Value("API Test") String appVersion){
        return new OpenAPI().info(
                new Info().title("CINEMA TICKET BINAR")
                        .version(appVersion)
                        .description(appDescription)
                        .termsOfService("http://swagger.io/terms")
                        .contact(new Contact()
                                .name("Vina Agustina").email("vinagstn25@gmail.com").url("https://gitlab.com/vinagustn"))
        );
    }
}
