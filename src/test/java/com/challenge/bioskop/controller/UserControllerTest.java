//package com.Challenge4.challenge4.controller;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@SpringBootTest
//class UserControllerTest {
//
//    @Autowired
//    private UserController userController;
//
//    @Test
//    @DisplayName("Positive Test Add User")
//    void addUserTest(){
//        String response = userController.addUser("shaqueenaaaa", "shanum12@gmail.com","shaqueena");
//        Assertions.assertEquals("Add User Success!",response);
//    }
//
//    @Test
//    @DisplayName("Positive Test Update")
//    public void updateAllTest(){
//        String response = userController.update("shanum", "pinakio12", "vinagustn222@gmail.com","bebekgoreng");
//        Assertions.assertEquals("Update username, email, and password Success!", response);
//    }
//
//    @Test
//    @DisplayName("Positive Test Delete User")
//    public void deleteUserTest(){
//        String response = userController.delete(4);
//        Assertions.assertEquals("Delete User Success!", response);
//    }
//
//}