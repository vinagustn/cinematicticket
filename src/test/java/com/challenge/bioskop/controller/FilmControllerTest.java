//package com.Challenge4.challenge4.controller;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//@SpringBootTest
//public class FilmControllerTest {
//
//    @Autowired
//    private FilmController filmController;
//
//    @Test
//    @DisplayName("Positive Test Add Film")
//    void addFilmTest(){
//        String response = filmController.addFilm("KKNT123","KKN Desa Penari", true);
//        Assertions.assertEquals("Add Film Success!", response);
//    }
//
//    @Test
//    @DisplayName("Positive Test Update Film Name")
//    void updateFilmNameTest(){
//        String response = filmController.updateFilmName("AFT003","After we Fell", false);
//        Assertions.assertEquals("Update Film Name Success!", response);
//    }
//
//    @Test
//    @DisplayName("Positive Test Delete Film")
//    void deleteFilmTest(){
//        String response = filmController.deleteFilm(8);
//        Assertions.assertEquals("Delete Film Success!", response);
//    }
//
//    @Test
//    @DisplayName("Positive Test Find Film is Show")
//    void findAllFilmsIsShowTest(){
//        filmController.findAllByIsShowTrue();
//    }
//
//    @Test
//    @DisplayName("Positive Test Save Schedules")
//    void saveSchedulesTest(){
//        String response = filmController.addSchedules(4, "2022-05-24","09:00:00","10:30:00","35000");
//        Assertions.assertEquals("Save Schedule Success!", response);
//    }
//
//    @Test
//    @DisplayName("Positive Test Show Schedules")
//    void findSchedulesByFilmIdTest(){
//        filmController.findSchedulesByFilmId(5);
//    }
//
//}
