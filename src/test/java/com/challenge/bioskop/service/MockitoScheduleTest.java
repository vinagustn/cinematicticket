package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Films;
import com.challenge.bioskop.model.Schedules;
import com.challenge.bioskop.repository.SchedulesRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MockitoScheduleTest {
    private ScheduleService scheduleService;

    @Mock
    private SchedulesRepository schedulesRepository;

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);
        this.scheduleService = new ScheduleServiceImpl(this.schedulesRepository);
    }

    @Test
    @DisplayName("Add Schedules Test")
    void testAddSchedules() {
        Schedules schedules = new Schedules();
        schedules.setSchedulesId(2);
        schedules.setDateShow("2022-05-27");
        schedules.setStartTime("20:00:00");
        schedules.setEndTime("22:50:00");
        schedules.setPrice(3500);
        Films films = new Films();
        films.setFilmId(1);

//        scheduleService.addSchedule(1, "2022-05-27", "20:00:00", "22:50:00", 3500);
//        Mockito.verify(schedulesRepository).save(schedules);
    }

}
