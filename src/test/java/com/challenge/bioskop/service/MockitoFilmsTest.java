package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Films;
import com.challenge.bioskop.repository.FilmsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
public class MockitoFilmsTest {
    @Mock
    private FilmsRepository filmsRepository;

    private FilmService filmService;

    @BeforeEach
    void init(){
        MockitoAnnotations.openMocks(this);
        this.filmService = new FilmServiceImpl(this.filmsRepository);
    }

    @Test
    @DisplayName("add films psitive test")
    void addFilmsTest(){
        Films films = new Films();
        films.setFilmId(10);
        films.setFilmCode("2020GHST");
        films.setFilmName("GHOST WRITER");
        films.setIsShow(true);

        assertThat(films.getFilmId()).isEqualTo(10);
        assertThat(films.getFilmCode()).isEqualTo("2020GHST");
        assertThat(films.getFilmName()).isEqualTo("GHOST WRITER");
        assertThat(films.getIsShow()).isTrue();

        filmService.addFilm(films);
        Mockito.verify(filmsRepository).save(films);
    }

    @Test
    @DisplayName("delete films positive test")
    void deleteFilmsTest(){
        Films films = new Films();
        films.setFilmId(10);

        filmService.deleteFilmById(films.getFilmId());
        Mockito.verify(filmsRepository).deleteById(films.getFilmId());
    }

    @Test
    @DisplayName("get all films test with stub")
    void stubGetAllFilms() {
        FilmService filmsService1 = Mockito.spy(new FilmServiceImpl(this.filmsRepository));
        List<Films> films = new ArrayList<>();
        Mockito.when(filmsService1.getAllFilms()).thenReturn(films);
        filmsService1.getAllFilms();
    }
}
