package com.challenge.bioskop.service;

import com.challenge.bioskop.model.Roles;
import com.challenge.bioskop.model.Users;
import com.challenge.bioskop.enumeration.ERole;
import com.challenge.bioskop.repository.UsersRepository;
import com.challenge.bioskop.controller.UserController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@ExtendWith(MockitoExtension.class)
public class MockitoUserTest {
    @Mock
    private UsersRepository usersRepository;

    private UserService userService;

    private UserController userController;

    @BeforeEach
    void init(){
        MockitoAnnotations.openMocks(this);
        this.userService = new UsersServiceImpl(this.usersRepository);
    }

    @Test
    @DisplayName("add users positive test")
    void saveUserTest(){
        Users users = new Users();
        users.setUserId(100);
        users.setUserName("Shaqueena");
        users.setEmail("shanum@gmail.com");
        users.setPassword("shanum123");

        Set<Roles> roles = new HashSet<>();
        roles.add(new Roles(1, ERole.CUSTOMER));
        users.setRoles(roles);

        assertThat(users.getUserName()).isEqualTo("Shaqueena");
        assertThat(users.getEmail()).isEqualTo("shanum@gmail.com");
        assertThat(users.getPassword()).isEqualTo("shanum123");
        assertThat(users.getRoles()).isEqualTo(users.getRoles());

        userService.saveUser(users);
        Mockito.verify(usersRepository).save(users);
    }

    @Test
    @DisplayName("updat users test")
    void testUpdateUsers() {
    }

    @Test
    @DisplayName("get all users test")
    void getAllUserTest(){
        userService.getAllUsers();
        Mockito.verify(usersRepository).findAll();
    }

    @Test
    @DisplayName("get user by username test")
    void getByUserNameTest(){
        Users users = new Users();
        users.setUserName("Vina");
        assertThat(users.getUserName()).isEqualTo("Vina");
    }

    @Test
    @DisplayName("get all user test with stub")
    void testStubGetAllUsers() {
        UserService usersService1 = Mockito.spy(new UsersServiceImpl(this.usersRepository));
        List<Users> users = new ArrayList<>();
        Mockito.when(usersService1.getAllUsers()).thenReturn(users);
        usersService1.getAllUsers();
    }

    @Test
    @DisplayName("find user by username test")
    void testMock() {
        Users expected = new Users();
        expected.setUserName("admin");
        Users users = usersRepository.findByUserName("admin");
    }

}
